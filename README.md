ASUS TUF Gaming FX505DT-AL027  
https://www.asus.com/ru/Laptops/For-Gaming/TUF-Gaming/ASUS-TUF-Gaming-FX505DD-DT-DU/

Guide for installition and configuration

***

Sources:
* Official asus.com drivers & soft https://www.asus.com/ru/Laptops/For-Gaming/TUF-Gaming/ASUS-TUF-Gaming-FX505DD-DT-DU/HelpDesk_Download/
* Official amd.com AMD Ryzen™ 7 3750H Driver https://www.amd.com/ru/support/apu/amd-ryzen-processors/amd-ryzen-7-mobile-processors-radeon-rx-vega-graphics/amd-ryzen-2
* Official amd.com AMD Ryzen™ and Athlon™ Mobile (Chipset) Driver https://www.amd.com/ru/support/chipsets/socket-fp5-mobile/amd-ryzen-and-athlon-mobile-chipset
* NVIDIA Driver for Video Card https://www.nvidia.com/Download/driverResults.aspx/184717/en-us

***

What you should to know:
1. Not all drivers, BIOSes and firmwares will be auto dounloaded by Windows 10 or Linux (Ubuntu) auto update systems. Another words only way to get ALL fresh drivers, BIOSes and firmwares is [MyASUS](https://www.asus.com/ru/support/MyASUS-deeplink/) program.
2. Windows 10 auto update system install wrong versions of AMD and Nvidia video drivers. So you have to install these drivers from official AMD and Nvidia sites.
3. Even ASUS official "Download drivers and BIOS" page has NOT ALL drivers and BIOSes. Only MyASUS utility has all of them (see below).

***

**Note!** you have to install AMD video driver after nvidia video driver! It is not necessary for newest video drivers' versions but you can do like that just in case.

Steps:
1. Install Windows 10, update system, pause system update to do not let Windows 10 override fresh manually installed driver versions.
2. Install Nvidia video driver https://www.nvidia.com/Download/driverResults.aspx/184717/en-us
    * Use Game Ready nvidia driver for Gaming despite it less stable it is good enough
    * When install the driver do **not** pick "install driver **with Experience**" install option, it is telemetry
3. Install AMD video driver https://www.amd.com/ru/support/apu/amd-ryzen-processors/amd-ryzen-7-mobile-processors-radeon-rx-vega-graphics/amd-ryzen-2
4. Install AMD Chipset driver https://www.amd.com/ru/support/chipsets/socket-fp5-mobile/amd-ryzen-and-athlon-mobile-chipset
5. Install [MyASUS](https://www.asus.com/ru/support/MyASUS-deeplink/)
6. Run MyASUS, go to **Customer Support > Live Update**, install all BIOSes and drivers
7. After installation from MyASUS you may need to reinstall AMD and Nvidia drivers because MyASUS may for example install Nvidia card VBIOS (ASUS VBIOS Flash Tool for FX505DT, it is only known way to install the Nvidia VBIOS).
8. Install TUF Aura Core https://www.microsoft.com/store/productId/9NNFL39XKXDN
9. **Optional! Install it if needed only!** (I see no difference of sound quality with DTS or without) Right click on speaker (sound volume) icon in tray. Then go to **Spacial Sound > DTS Sound Unbound**, install **DTS Sound Unbound**. Or use direct link: [DTS Sound Unbound](https://www.microsoft.com/store/productId/9PJ0NKL8MCSJ)

***

Configuration:
1. Unlock Turbo Boost control, run cmd as Administrator and run the command below
    ```cmd
    powercfg -attributes 54533251-82be-4824-96c1-47b60b740d00 be337238-0d82-4146-a960-4f3749d470c7 -ATTRIB_HIDE
    ```
2. To reduce noise Create second power plan with **Turbo Boost disabled** and set **Max CPU frequency to 99%** and use it when needed
3. Enable Hibernation. Open Control Panel, go to **Control Panel\Hardware and Sound\Power Options\System Settings**. Disable **"Turn on fast start up"** (to allow change Hard Disk Partitions when needed), enable Hibernate in **Power Button menu**.
